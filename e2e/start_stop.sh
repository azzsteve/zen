#!/usr/bin/env bash

set -eo pipefail

function set_up() {
    mkdir -p ~/.zen
    cat >~/.zen/config.toml <<EOL
domains = [
    "example.com"
]
EOL
}

function run() {
    echo "==============="
    echo "zen start"
    echo "==============="
    zen start
    echo "Checking hosts file"
    grep "example.com" /etc/hosts
    echo "Checking example.com ip"
    ping -c1 example.com | grep "127.0.0.1"

    echo "==============="
    echo "zen stop"
    echo "==============="
    zen stop 
    if grep -q "example.com" /etc/hosts; then
        echo "Host file not empty"
        exit 1
    fi
    ping_out=$(ping -c1 example.com)
    if grep -q "127.0.0.1" <<< $ping_out; then
        echo "example.com shouldn't have resolved to 127.0.0.1"
        exit 1
    fi
}

function tear_down() {
    zen stop
    rm -rf ~/.zen
}
trap tear_down EXIT

set_up
run
