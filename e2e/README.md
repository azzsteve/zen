# End to end tests

This directory hosts scripts where it assumes that it has `zen` inside the
`$PATH` and can execute it in an isolated environment to run commands and
validate that everything works as expected.

At the moment each test is executed sequentially and not in parllel for
simplicy reasons. Each test is expected to clean up resources after itself.

---

*This is an expermiment that I'm trying and it might be a horrible idea on how
to run end to end tests.*
