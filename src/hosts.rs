use std::{
    fs::{copy, File, OpenOptions},
    io::{BufRead, BufReader, Write},
    path::PathBuf,
    writeln,
};

use anyhow::{Context, Result};

const START_BLOCK: &str = "# Start of Zen";
const END_BLOCK: &str = "# End of Zen";

pub fn block(backup_dir: &PathBuf, path: &PathBuf, domains: &[String]) -> Result<()> {
    unblock(backup_dir, path).context("Removing existing blocks if present")?;

    let mut hosts_file = OpenOptions::new()
        .append(true)
        .open(path)
        .context("Opening host file to block domains")?;

    writeln!(hosts_file, "{}", START_BLOCK)?;
    for d in domains {
        writeln!(hosts_file, "127.0.0.1 {}", d)?;
    }
    writeln!(hosts_file, "{}", END_BLOCK)?;

    Ok(())
}

pub fn unblock(backup_dir: &PathBuf, hosts_path: &PathBuf) -> Result<()> {
    let hosts_backup_path = backup_dir.join("hosts.bak");
    copy(&hosts_path, &hosts_backup_path).context("Creating hosts backup file")?;

    println!(
        "Created backup file of hosts file at {:?}",
        hosts_backup_path
    );

    let hosts_backup = OpenOptions::new()
        .read(true)
        .open(hosts_backup_path)
        .context("Opening host backup file to unblock domains")?;

    let mut hosts = File::create(hosts_path).context("Opening host file to unblock domains")?;

    let reader = BufReader::new(&hosts_backup);

    let mut zen_block: bool = false;
    for (_index, line) in reader.lines().enumerate() {
        let line = line?;

        if line == START_BLOCK {
            zen_block = true
        }

        if !zen_block {
            writeln!(hosts, "{}", line).context("Writing to hosts file")?;
        }

        if line == END_BLOCK {
            zen_block = false
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fs::{self, File};

    use tempfile::tempdir;

    use super::*;

    #[test]
    fn test_block() {
        let mut block_domains = vec![String::from("example.com"), String::from("example.net")];
        let dir = tempdir().expect("tmpdir failed");
        let tmp_hosts = dir.path().join("hosts");

        File::create(&tmp_hosts).expect("create tmp hosts file failed");

        // First Block
        block(&dir.path().to_path_buf(), &tmp_hosts, &block_domains).unwrap();
        let host_contents = fs::read_to_string(&tmp_hosts).unwrap();
        let expected_host_contents = format!(
            "{}
127.0.0.1 example.com
127.0.0.1 example.net
{}
",
            START_BLOCK, END_BLOCK
        );
        assert_eq!(expected_host_contents, host_contents);

        // Second block should leave the file as it is.
        block(&dir.path().to_path_buf(), &tmp_hosts, &block_domains).unwrap();
        let host_contents = fs::read_to_string(&tmp_hosts).unwrap();
        assert_eq!(expected_host_contents, host_contents);

        // Third block that adds more domains
        block_domains.append(&mut vec![String::from("example.org")]);
        block(&dir.path().to_path_buf(), &tmp_hosts, &block_domains).unwrap();
        let expected_host_contents = format!(
            "{}
127.0.0.1 example.com
127.0.0.1 example.net
127.0.0.1 example.org
{}
",
            START_BLOCK, END_BLOCK
        );
        let host_contents = fs::read_to_string(&tmp_hosts).unwrap();
        assert_eq!(expected_host_contents, host_contents);

        drop(tmp_hosts);
        dir.close().unwrap();
    }

    #[test]
    fn test_unblock() {
        let hosts_original_content = format!(
            "127.0.0.1 localhost
192.168.1.1 local.domain
# A comment
{}
127.0.0.1 example.com
127.0.0.1 example.net
{}
192.168.190.160 test.domain
# Another comment",
            START_BLOCK, END_BLOCK
        );

        let expected_hosts_content = String::from(
            "127.0.0.1 localhost
192.168.1.1 local.domain
# A comment
192.168.190.160 test.domain
# Another comment
",
        );

        let dir = tempdir().expect("tmpdir failed");

        let hosts_path = dir.path().join("hosts");
        let mut hosts = File::create(&hosts_path).expect("failed to create host file");
        write!(hosts, "{}", hosts_original_content).unwrap();

        unblock(&dir.path().to_path_buf(), &hosts_path).unwrap();

        assert!(dir.path().join("hosts.bak").exists());

        let hosts_bak_content = fs::read_to_string(dir.path().join("hosts.bak"))
            .expect("Failed to read hosts back up file");
        assert_eq!(hosts_original_content, hosts_bak_content);

        let hosts_content =
            fs::read_to_string(hosts_path).expect("Failed to read hosts back up file");
        assert_eq!(expected_hosts_content, hosts_content);

        dir.close().unwrap();
    }
}
