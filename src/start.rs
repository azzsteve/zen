use std::path::Path;

use clap::{App, SubCommand};

use crate::config::{read_config, zen_dir};
use crate::hosts::block;

// START_CMD is the name of the start sub command.
pub const START_CMD: &str = "start";

// start_cmd return a new clap subcommand
pub fn start_cmd<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(START_CMD).about("Start blocking things")
}

// start runs the sub command.
pub fn start() {
    let zen_dir = zen_dir().expect("Failed to get zen directory");
    let config = read_config(&zen_dir).expect("Failed to read config file");

    block(
        &zen_dir,
        &Path::new("/etc/hosts").to_path_buf(),
        &config.domains,
    )
    .unwrap();
}
